let firstName = "John";
let lastName = "Smith";
let age = 30;
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
let workAddress = {
  houseNumber: "32",
  street: "Washington",
  city: "Lincoln",
  state: "Nebraska",
};

// 1. Create a variable called "fullName" that concatenates firstName and lastName.
let fullName = `${firstName} ${lastName}`;

// 2. Create a variable called "address" that concatenates workAddress.
let { houseNumber, street, city, state } = workAddress;
let address = `${houseNumber} ${street}, ${city}, ${state}`;

// Results
console.log("First Name:", firstName);
console.log("Last Name:", lastName);
console.log("Full Name:", fullName);
console.log("Age:", age);
console.log("Hobbies:", hobbies);
console.log("Work Address", workAddress);

fullName = "Steve Rogers";
age = 40;
let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
let fullProfile = {
  username: "captain_america",
  fullName: "Steve Rogers",
  age: 40,
  isActive: false,
};
let bestfriend = "Bucky Barnes";
let placeFound = "Arctic Ocean";

// Results 2
console.log("My full name is:", fullName);
console.log("My current age is:", age);
console.log("My Friends are:", friends);
console.log("My Full Profile:", fullProfile);
console.log("My bestfriend is:", bestfriend);
console.log("I was found frozen in:", placeFound);
